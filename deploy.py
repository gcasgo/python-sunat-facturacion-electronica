from decouple import config
from fabric import Connection


host = 'pytel@' + config('SERVER_ADDRESS')
password = config('SERVER_PASSWORD')
deploy_token = config('DEPLOY_TOKEN')
PROJECT_NAME = 'cataleya'
PROJECT_DIR = '~/webapps/cataleya/src'
VENV = 'source ~/webapps/cataleya/env/bin/activate'


with Connection(host, connect_kwargs={ "password": password }) as conn:
    dir_change = conn.run('cd %s' % PROJECT_DIR, warn=True)
    if dir_change.failed:
        conn.run(
            (
            'git clone '
            'https://gitlab+deploy-token-237599:{}@gitlab.com'
            '/gcasgo/cataleya.git' # replace with Gitlab repo
            ).format(deploy_token)
        )

    with conn.cd(PROJECT_DIR):
        conn.run('git pull origin master')

        with conn.prefix(VENV):
            conn.run('pip install -r ../requirements.txt')
            conn.run('python manage.py migrate')
            conn.run('python manage.py collectstatic -l --noinput')

        conn.run("supervisorctl restart %s" % PROJECT_NAME)
        conn.run("supervisorctl restart %s-bot" % PROJECT_NAME)
