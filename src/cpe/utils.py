import locale


def cantidad_en_letras(value):
    en_letras = EnLetras()
    return en_letras.valor_en_letras(value, "SOLES")


class EnLetras:

    void = ""
    espacio = " "
    dot = "."
    zero = "0"
    neg = "Menos"

    def valor_en_letras(self, value, Moneda):
        Signo = self.neg + " " if float(value) < 0 else ""

        text = locale.format_string("%.*f", (2, value), True)

        if self.dot in text:
            Ent = text.split(self.dot)[0]
            Frc =  text.split(self.dot)[1]
        else:
            Ent = text
            Frc = self.void

        if Ent == self.zero or Ent == self.void:
            text = "CERO "

        elif len(Ent) > 7:
            millones = Ent[::(len(Ent) - 6)]
            miles = Ent[-6::]
            text = self.SubValLetra(millones) + "MILLONES " + self.SubValLetra(miles)

        else:
            text = self.SubValLetra(Ent)

        if text[-9:] == "MILLONES " or text[-7:] == "MILLÓN ":
            text += "DE "

        if Frc != self.void:
            text = text + "CON " + Frc + "/100"

        text = Signo + text + " " + Moneda
        return text

    def SubValLetra(self, numero):
        text = ""

        numero_str = str(numero).strip()

        n = len(numero_str)
        i = n
        Tem = self.void
        while i > 0:
            step = numero_str[n - i: n - i + 1] + self.zero * (i - 1)
            Tem = self.Parte(step)
            if Tem != "CERO":
                text += Tem + self.espacio
            i = i - 1

        # --------------------- GoSub FiltroMil ------------------------------
        text = text.replace(" MIL MIL", " UN MIL")
        while True:
            index = text.find("MIL ")
            if index != -1:
                if text.find("MIL ", index + 1) != -1:
                    text = self.ReplaceStringFrom(text, "MIL ", "", index)
                else:
                    break
            else:
                break

        # --------------------- GoSub FiltroCiento ------------------------------
        index = -1
        while True:
            index = text.find("CIEN ", index + 1)
            if index != -1:
                Tem = text[index + 5 : index + 5 + 1]
                if Tem == "M" or Tem == self.void:
                    pass
                else:
                    text = self.ReplaceStringFrom(text, "CIEN", "CIENTO", index)

            if index == -1:
                break

        # --------------------- FiltroEspeciales ------------------------------
        text = text.replace("DIEZ UNO", "ONCE")
        text = text.replace("DIEZ UNO", "ONCE")
        text = text.replace("DIEZ DOS", "DOCE")
        text = text.replace("DIEZ TRES", "TRECE")
        text = text.replace("DIEZ CUATRO", "CATORCE")
        text = text.replace("DIEZ CINCO", "QUINCE")
        text = text.replace("DIEZ SEIS", "DIECISEIS")
        text = text.replace("DIEZ SIETE", "DIECISIETE")
        text = text.replace("DIEZ OCHO", "DIECIOCHO")
        text = text.replace("DIEZ NUEVE", "DIECINUEVE")
        text = text.replace("VEINTE UN", "VEINTIUN")
        text = text.replace("VEINTE DOS", "VEINTIDOS")
        text = text.replace("VEINTE TRES", "VEINTITRES")
        text = text.replace("VEINTE CUATRO", "VEINTICUATRO")
        text = text.replace("VEINTE CINCO", "VEINTICINCO")
        text = text.replace("VEINTE SEIS", "VEINTISEIS")
        text = text.replace("VEINTE SIETE", "VEINTISIETE")
        text = text.replace("VEINTE OCHO", "VEINTIOCHO")
        text = text.replace("VEINTE NUEVE", "VEINTINUEVE")

        # --------------------- FiltroUn ------------------------------
        # if text[0:1] == "M":
        #     text = " " + text

        # --------------------- Adicionar Y ------------------------------
        for i in range(65, 89):
            if i != 77:
                text = text.replace("A " + chr(i), "* Y " + chr(i))

        text = text.replace("*", "A")
        return text

    def ReplaceStringFrom(self, x, OldWrd, NewWrd, Ptr):
        # $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr);
        x = x[0:Ptr] + NewWrd + x[len(OldWrd) + Ptr::]
        return x

    def Parte(self, x):
        x = int(x)
        t = ''
        i = 0
        while True:
            if x == 0:
                t = "CERO"
                break
            elif x == 1:
                t = "UNO"
                break
            elif x == 2:
                t = "DOS"
                break
            elif x == 3:
                t = "TRES"
                break
            elif x == 4:
                t = "CUATRO"
                break
            elif x == 5:
                t = "CINCO"
                break
            elif x == 6:
                t = "SEIS"
                break
            elif x == 7:
                t = "SIETE"
                break
            elif x == 8:
                t = "OCHO"
                break
            elif x == 9:
                t = "NUEVE"
                break
            elif x == 10:
                t = "DIEZ"
                break
            elif x == 20:
                t = "VEINTE"
                break
            elif x == 30:
                t = "TREINTA"
                break
            elif x == 40:
                t = "CUARENTA"
                break
            elif x == 50:
                t = "CINCUENTA"
                break
            elif x == 60:
                t = "SESENTA"
                break
            elif x == 70:
                t = "SETENTA"
                break
            elif x == 80:
                t = "OCHENTA"
                break
            elif x == 90:
                t = "NOVENTA"
                break
            elif x == 100:
                t = "CIEN"
                break
            elif x == 200:
                t = "DOSCIENTOS"
                break
            elif x == 300:
                t = "TRESCIENTOS"
                break
            elif x == 400:
                t = "CUATROCIENTOS"
                break
            elif x == 500:
                t = "QUINIENTOS"
                break
            elif x == 600:
                t = "SEISCIENTOS"
                break
            elif x == 700:
                t = "SETECIENTOS"
                break
            elif x == 800:
                t = "OCHOCIENTOS"
                break
            elif x == 900:
                t = "NOVECIENTOS"
                break
            elif x == 1000:
                t = "MIL"
                break
            elif x == 1000000:
                t = "MILLÓN"
                break

            if t == self.void:
                i = i + 1
                x = x / 1000
                if x == 0:
                    i = 0
            else:
                break

            if x == 0:
                break

        if i == 0:
            t += self.void
        elif i == 1:
            t += " MIL"
        elif i == 2:
            t += " MILLONES"
        elif i == 3:
            t += " BILLONES"

        return t

