from datetime import date

from .utils import cantidad_en_letras

emisor = {
    'tipodoc'                   :  '6',
    'ruc'                       :  '20123456789',
    'razon_social'              :  'CETI ORG',
    'nombre_comercial'          :  'CETI',
    'direccion'                 :  'VIRTUAL',
    'ubigeo'                    :  '130101',
    'departamento'              :  'LAMBAYEQUE',
    'provincia'                 :  'CHICLAYO',
    'distrito'                  :  'CHICLAYO',
    'pais'                      :  'PE',
    'usuario_secundario'        :  'MODDATOS',
    'clave_usuario_secundario'  :  'MODDATOS'
}

cliente = {
    'tipodoc'                   :  '6',
    'ruc'                       :  '10123456789',
    'razon_social'              :  'CLIENTE DE PRUEBA',
    'direccion'                 :  'VIRTUAL',
    'pais'                      :  'PE'
}

comprobante = {
    'tipodoc'                   :  '01',
    'serie'                     :  'F001',
    'correlativo'               :  124,
    'fecha_emision'             :  date.today().strftime('Y-m-d'),
    'hora'                      :  '00:00:00',
    'fecha_vencimiento'         :  date.today().strftime('Y-m-d'),
    'moneda'                    :  'PEN',  #//PEN: SOLES, USD: DOLARES
    'total_opgravadas'          :  0,
    'total_opexoneradas'        :  0,
    'total_opinafectas'         :  0,
    'total_impbolsas'           :  0,
    'igv'                       :  0,
    'total'                     :  0,
    'total_texto'               :  '',
    'forma_pago'                :  'Credito',
    'monto_pendiente'           :  100,
}

cuotas = [
    {
        'cuota'                 :  'Cuota001',
        'monto'                 :  50,
        'fecha'                 :  '2022-03-12'
    },
    {
        'cuota'                 :  'Cuota002',
        'monto'                 :  50,
        'fecha'                 :  '2022-04-12'
    },
]

detalle = [
    {
        'item'                  :  1,
        'codigo'                :  'COD001',
        'descripcion'           :  'BICICLETA GOLIAT SIERRA 29',
        'cantidad'              :  1,
        'valor_unitario'        :  1016.95,  #No incluye IGV
        'precio_unitario'       :  1200,  #incluye IGV=18%
        'tipo_precio'           :  '01',  #catalogo nro 16
        'igv'                   :  183.05,
        'porcentaje_igv'        :  18,
        'valor_total'           :  1016.95,  #cantidad * valor unitario
        'importe_total'         :  1200,  #cantidad * precio unitario
        'unidad'                :  'NIU',
        'tipo_afectacion_igv'   :  '10',  #10:gravadas, 20; exoneradas, 30: inafectas
        'codigo_tipo_tributo'   :  '1000',
        'tipo_tributo'          :  'VAT',
        'nombre_tributo'        :  'IGV',
        'bolsa_plastica'        :  'NO'
    },
    {
        'item'                  :  2,
        'codigo'                :  'COD002',
        'descripcion'           :  'LIBRO MATEMATICA',
        'cantidad'              :  2,
        'valor_unitario'        :  120,  #No incluye IGV
        'precio_unitario'       :  120,  #incluye IGV=18%
        'tipo_precio'           :  '01',  #catalogo nro 16
        'igv'                   :  0,
        'porcentaje_igv'        :  18,
        'valor_total'           :  240,  #cantidad * valor unitario
        'importe_total'         :  240,  #cantidad * precio unitario
        'unidad'                :  'NIU',
        'tipo_afectacion_igv'   :  '20',  #10:gravadas, 20; exoneradas, 30: inafectas
        'codigo_tipo_tributo'   :  '9997',
        'tipo_tributo'          :  'VAT',
        'nombre_tributo'        :  'EXO',
        'bolsa_plastica'        :  'NO'
    },
    {
        'item'                  :  3,
        'codigo'                :  'COD003',
        'descripcion'           :  'SANDIA',
        'cantidad'              :  1,
        'valor_unitario'        :  8,  #No incluye IGV
        'precio_unitario'       :  8,  #incluye IGV=18%
        'tipo_precio'           :  '01',  #catalogo nro 16
        'igv'                   :  0,
        'porcentaje_igv'        :  18,
        'valor_total'           :  8,  #cantidad * valor unitario
        'importe_total'         :  8,  #cantidad * precio unitario
        'unidad'                :  'NIU',
        'tipo_afectacion_igv'   :  '30',  #10:gravadas, 20; exoneradas, 30: inafectas
        'codigo_tipo_tributo'   :  '9998',
        'tipo_tributo'          :  'FRE',
        'nombre_tributo'        :  'INA',
        'bolsa_plastica'        :  'NO'
    },
    {
        'item'                  :  4,
        'codigo'                :  'CODBOL',
        'descripcion'           :  'BOLSA PLASTICA',
        'cantidad'              :  4,
        'valor_unitario'        :  0.04,  #No incluye IGV
        'precio_unitario'       :  0.05,  #incluye IGV=18%
        'tipo_precio'           :  '01',  #catalogo nro 16
        'igv'                   :  0.04,
        'porcentaje_igv'        :  18,
        'valor_total'           :  0.16,  #cantidad * valor unitario
        'importe_total'         :  0.20,  #cantidad * precio unitario
        'unidad'                :  'NIU',
        'tipo_afectacion_igv'   :  '10',  #10:gravadas, 20; exoneradas, 30: inafectas
        'codigo_tipo_tributo'   :  '1000',
        'tipo_tributo'          :  'VAT',
        'nombre_tributo'        :  'IGV',
        'bolsa_plastica'        :  'SI'
    }
]

# //inicializar totales
total_opgravadas = 0;
total_opexoneradas = 0;
total_opinafectas = 0;
total_impbolsas = 0;
igv = 0;
total = 0;

for value in detalle:
    if(value['tipo_afectacion_igv'] == 10):
        total_opgravadas += value['valor_total']

    if(value['tipo_afectacion_igv'] == 20):
        total_opexoneradas += value['valor_total']

    if(value['tipo_afectacion_igv'] == 30):
        total_opinafectas += value['valor_total']

    if (value['bolsa_plastica'] == 'SI') :
        total_impbolsas += value['cantidad']*0.40  # //el valor de 0.40 corresponde al año 2022


    igv += value['igv']
    total += value['importe_total'] + total_impbolsas


comprobante['total_opgravadas'] = total_opgravadas
comprobante['total_opexonerdas'] = total_opexoneradas
comprobante['total_opinafectas'] = total_opinafectas
comprobante['igv'] = igv
comprobante['total'] = total
comprobante['total_impbolsas'] = total_impbolsas

comprobante['total_texto'] = cantidad_en_letras(total}

# //PASO 01: CREAR EL XML DE FACTURA - INICIO

# //Nombre del XML
# //Nomenclatura de SUNAT: RUC EMISOR - TIPO COMPRABANTE - SERIE - CORRELATIVO . XML
# //Ejemplo: 20123456789-01-F001-1.XML

# //PASO 01 - FIN
