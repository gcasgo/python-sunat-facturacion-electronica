from django.test import TestCase

from .utils import cantidad_en_letras

assert cantidad_en_letras(10000) == "DIEZ MIL CON 00/100 SOLES"
assert cantidad_en_letras(5000) == "CINCO MIL CON 00/100 SOLES"
assert cantidad_en_letras(1005) == "MIL CINCO CON 00/100 SOLES"

assert cantidad_en_letras(0) == "CERO CON 00/100 SOLES"
assert cantidad_en_letras(1) == "UNO CON 00/100 SOLES"

assert cantidad_en_letras(15) == "QUINCE CON 00/100 SOLES"
assert cantidad_en_letras(10) == "DIEZ CON 00/100 SOLES"
assert cantidad_en_letras(91) == "NOVENTA Y UNO CON 00/100 SOLES"

assert cantidad_en_letras(105) == "CIENTO CINCO CON 00/100 SOLES"
assert cantidad_en_letras(151) == "CIENTO CINCUENTA Y UNO CON 00/100 SOLES"
assert cantidad_en_letras(215) == "DOSCIENTOS QUINCE CON 00/100 SOLES"
