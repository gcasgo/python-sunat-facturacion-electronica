from cpe.utils import cantidad_en_letras

for number in range(0, 1000):
    text = cantidad_en_letras(number)
    print(f"{number} -> '{text}'")
